﻿using System.Collections.Generic;

namespace dk.zunk.RemoteAttachIis
{
  public class IisInfo
  {
    public List<ApplicationInfo> Applications { get; set; }
  }
}