﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace dk.zunk.RemoteAttachIis
{
  [Serializable]
  public class Server : INotifyPropertyChanged
  {
    private const string DefaultSuffix = ":4016";
    private static readonly ISet<string> Locals = new HashSet<string>(new[] { "localhost", "127.0.0.1", "::1" });

    private string _host;
    private string _qualifier;

    public string Host
    {
      get { return _host; }
      set
      {
        _host = (value ?? "").Trim();
        if (string.IsNullOrWhiteSpace(Qualifier) && !string.IsNullOrWhiteSpace(_host))
          Qualifier = GetTransportQualifier(_host);
        OnPropertyChanged("Host");
      }
    }

    [Description("Default port for MSVSMON is 4016 (fx example.com:4016)")]
    public string Qualifier
    {
      get { return _qualifier; }
      set {
        _qualifier = (value ?? "").Trim();
        OnPropertyChanged("Qualifier");
      }
    }

    public override string ToString()
    {
      if (Qualifier == null)
        return Host ?? "";
      return Qualifier.EndsWith(":4016")
          ? Qualifier.Substring(0, Qualifier.Length - DefaultSuffix.Length)
          : Qualifier;
    }

    private static string GetTransportQualifier(string server)
    {
      return Locals.Contains(server.ToLowerInvariant())
        ? server
        : server + ":4016";
    }

    public event PropertyChangedEventHandler PropertyChanged;

    private void OnPropertyChanged(String info)
    {
      var handler = PropertyChanged;
      if (handler != null)
        handler(this, new PropertyChangedEventArgs(info));
    }
  }
}
