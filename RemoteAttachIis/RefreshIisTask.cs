﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.VisualStudio;
using Microsoft.VisualStudio.Shell;
using Microsoft.VisualStudio.Shell.Interop;

namespace dk.zunk.RemoteAttachIis
{
  sealed class RefreshIisTask : IDisposable
  {
    private readonly BackgroundWorker _backgroundWorker;

    public delegate void CompletedIisInfo(object sender, IisInfo iisInfo);
    public event CompletedIisInfo Completed;
    public event EventHandler Started;

    private void OnStarted()
    {
      var handler = Started;
      if (handler != null) handler(this, EventArgs.Empty);
    }

    private void OnCompleted(IisInfo iisinfo)
    {
      var handler = Completed;
      if (handler != null) handler(this, iisinfo);
    }

    public RefreshIisTask()
    {
      _backgroundWorker = new BackgroundWorker();
      _backgroundWorker.DoWork += GetRemoteIisInfo;
      _backgroundWorker.RunWorkerCompleted += GetRemoteIisInfoCompleted;
    }

    public void StartAsync(string server)
    {
      OnStarted();
      _backgroundWorker.RunWorkerAsync(server);
    }

    private void GetRemoteIisInfoCompleted(object sender, RunWorkerCompletedEventArgs args)
    {
      if (args.Error != null)
      {
        ActivityLog.LogError(typeof(RemoteAttachIisPackage).Name, args.Error.ToString());
        var outputWindow = Package.GetGlobalService(typeof(SVsOutputWindow)) as IVsOutputWindow;

        var guidGeneral = VSConstants.OutputWindowPaneGuid.GeneralPane_guid;
        if (outputWindow == null) return;
        if (outputWindow.CreatePane(guidGeneral, "General", 1, 0) != VSConstants.S_OK) return;
        IVsOutputWindowPane pane;
        if (outputWindow.GetPane(guidGeneral, out pane) != VSConstants.S_OK) return;
        pane.Activate();
        pane.OutputString(args.Error.ToString());
        OnCompleted(new IisInfo { Applications = new List<ApplicationInfo>(0) });
      }
      else
      {
        OnCompleted((IisInfo)args.Result);
      }
    }

    private static void GetRemoteIisInfo(object sender, DoWorkEventArgs args)
    {
      args.Result = RemoteIis.GetRemoteIisInfo((string)args.Argument);
    }

    public void Dispose()
    {
      if (_backgroundWorker == null) return;
      _backgroundWorker.Dispose();
    }
  }
}