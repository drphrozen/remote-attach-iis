﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;

namespace dk.zunk.RemoteAttachIis
{
  [Serializable]
  public class ServerList
  {
    public static readonly ServerList DefaultList = new ServerList(new List<Server> { new Server { Host = "localhost", Qualifier = "localhost" } });

    public ServerList() : this(new List<Server>()) { }

    public ServerList(List<Server> servers)
    {
      if (servers == null) throw new ArgumentNullException("servers");
      _servers = servers;
      _bindingList = new BindingList<Server>(_servers);
    }

    private readonly List<Server> _servers;
    private readonly BindingList<Server> _bindingList;

    [XmlArray("Servers")]
    [XmlArrayItem("Server")]
    public List<Server> Servers
    {
      get { return _servers; }
      protected set
      {
        _servers.Clear();
        _servers.AddRange(value);
        _bindingList.ResetBindings();
      }
    }

    [XmlIgnore]
    public BindingList<Server> Items
    {
      get { return _bindingList; }
      set { ClearAndAddRange(value); }
    }

    public void ClearAndAddRange(IEnumerable<Server> list)
    {
      _servers.Clear();
      _servers.AddRange(list);
      _bindingList.ResetBindings();
    }
  }

}
