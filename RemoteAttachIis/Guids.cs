﻿using System;

namespace dk.zunk.RemoteAttachIis
{
  static class GuidList
  {
    public const string GuidRemoteAttachIisPkgString = "bd2e30cb-91ab-4693-81ff-d2453a1889da";
    private const string GuidRemoteAttachIisCommandSetString = "ca96ff0a-537c-41f1-9b47-f42e9639b977";
    //public const string GuidToolWindowPersistanceString = "6d854a47-ece7-438f-aaf8-19c7de89ee8a";
    public const string GuidOptionsDialogString = "ddf3077e-8bfe-409e-a446-4638b2247744";
    public static readonly Guid GuidRemoteAttachIisCommandSet = new Guid(GuidRemoteAttachIisCommandSetString);
  };
}