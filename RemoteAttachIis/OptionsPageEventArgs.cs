﻿using System;

namespace dk.zunk.RemoteAttachIis
{
  public class OptionsPageEventArgs : EventArgs
  {
    public OptionPageCustom OptionsPage { get; private set; }

    public OptionsPageEventArgs(OptionPageCustom optionsPage)
    {
      OptionsPage = optionsPage;
    }
  }
}