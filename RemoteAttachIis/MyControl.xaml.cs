﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace dk.zunk.RemoteAttachIis
{
  /// <summary>
  /// Interaction logic for MyControl.xaml
  /// </summary>
  public partial class MyControl : IDisposable
  {
    private IisInfo _iisInfo;
    private RefreshIisTask _refreshIisTask;

    public OptionPageCustom OptionsPage { get; set; }

    public MyControl()
    {
      InitializeComponent();

      _refreshIisTask = new RefreshIisTask();
      _refreshIisTask.Started += RefreshIisTaskOnStarted;
      _refreshIisTask.Completed += RefreshIisTaskOnCompleted;


      Servers.SelectionChanged += ServersOnSelectionChanged;

      var optionsPage = RemoteAttachIisPackage.Instance.GetOptionsPage();
      optionsPage.OptionsChanged += ApplyOptions;
      ApplyOptions(this, new OptionsPageEventArgs(optionsPage));
    }

    private void RefreshIisTaskOnCompleted(object sender, IisInfo iisInfo)
    {
      _iisInfo = iisInfo;
      IisInfoDataGrid.DataContext = _iisInfo.Applications;
      IisInfoDataGrid.Items.Refresh();
      ProgressBar.Visibility = Visibility.Collapsed;
      IisInfoDataGrid.Visibility = Visibility.Visible;
    }

    private void RefreshIisTaskOnStarted(object sender, EventArgs eventArgs)
    {
      ProgressBar.Visibility = Visibility.Visible;
      IisInfoDataGrid.Visibility = Visibility.Collapsed;
    }

    private void ServersOnSelectionChanged(object sender, SelectionChangedEventArgs args)
    {
      var server = args.AddedItems.Cast<Server>().FirstOrDefault();
      if (server == null) return;
      TransportQualifier.Text = server.Qualifier;
    }

    private void ApplyOptions(object sender, OptionsPageEventArgs args)
    {
      Servers.ItemsSource = (args.OptionsPage.Servers ?? (new ServerList (new List<Server>()))).Items;

      if (Servers.Items.Count > 0)
      {
        Servers.SelectedIndex = 0;
      }
    }

    private Server GetSelectedItem()
    {
      return Servers.SelectedItem as Server;
    }

    private void Attach(object sender, RoutedEventArgs e)
    {
      var applicationInfo = GetApplicationInfo(sender);
      if (applicationInfo == null) return;
      var server = GetSelectedItem();
      if (server == null) return;
      try
      {
        RemoteAttachIisPackage.Instance.GetDebugger().Attach(server.Host, applicationInfo.ApplicationPoolName, server.Qualifier);
      }
      catch (Exception ex)
      {
        MyModalDialog.ShowModal("Could not attach", ex.Message);
      }
    }

    private void RefreshButtonClick(object sender, RoutedEventArgs e)
    {
      RefreshServer();
    }

    private void RefreshServer()
    {
      var server = GetSelectedItem();
      if (server == null) return;
      _iisInfo = new IisInfo { Applications = new List<ApplicationInfo>(0) };

      _refreshIisTask.StartAsync(server.Host);
    }

    private void ServersSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      RefreshServer();
    }

    private void Recycle(object sender, RoutedEventArgs e)
    {
      var applicationInfo = GetApplicationInfo(sender);
      var server = GetSelectedItem();
      if (server == null) return;
      try
      {
        RemoteIis.Recycle(server.Host, applicationInfo.ApplicationPoolName);
      }
      catch (Exception ex)
      {
        MyModalDialog.ShowModal("Could not recycle", ex.Message);
      }
    }

    private static ApplicationInfo GetApplicationInfo(object sender)
    {
      ApplicationInfo applicationInfo = null;
      for (var vis = sender as Visual; vis != null; vis = VisualTreeHelper.GetParent(vis) as Visual)
      {
        var row = vis as DataGridRow;
        if (row == null) continue;
        applicationInfo = row.Item as ApplicationInfo;
        break;
      }
      return applicationInfo;
    }

    public void Dispose()
    {
      if (_refreshIisTask == null) return;
      _refreshIisTask.Dispose();
      _refreshIisTask = null;
    }

    private void OptionsButtonClick(object sender, RoutedEventArgs e)
    {
      RemoteAttachIisPackage.Instance.ShowOptionPage(typeof(OptionPageCustom));
    }
  }
}