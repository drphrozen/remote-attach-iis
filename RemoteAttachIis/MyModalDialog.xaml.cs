﻿using System.Windows;

namespace dk.zunk.RemoteAttachIis
{

  // Use this constructor to enable F1 Help. 
  public partial class MyModalDialog
  {
    // Use this constructor to provide a Help button and F1 support. 
    public MyModalDialog(string helpTopic)
      : base(helpTopic)
    {
      InitializeComponent();
    }

    // Use this constructor for minimize and maximize buttons and no F1 Help. 
    public MyModalDialog()
    {
      HasMaximizeButton = false;
      HasMinimizeButton = false;
      InitializeComponent();
    }

    public static bool? ShowDialog(string title, string message)
    {
      return new MyModalDialog { Message = message, Title = title }.ShowDialog();
    }

    public static void Show(string title, string message)
    {
      new MyModalDialog { Message = message, Title = title }.Show();
    }

    public static bool? ShowModal(string title, string message)
    {
      return new MyModalDialog { Message = message, Title = title }.ShowModal();
    }

    private void OkClick(object sender, RoutedEventArgs e)
    {
      Close();
    }

    public string Message { get { return TextBlock.Text; } set { TextBlock.Text = value; } }
  }
}
