﻿namespace dk.zunk.RemoteAttachIis
{
  public class ApplicationInfo
  {
    public string ApplicationPath { get; set; }
    public string SiteName { get; set; }
    public int Pid { get; set; }
    public string ApplicationPoolName { get; set; }
    public string Bindings { get; set; }
    public string Server { get; set; }
  }
}