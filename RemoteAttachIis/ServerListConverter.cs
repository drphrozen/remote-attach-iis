﻿using System;
using System.ComponentModel;
using System.IO;
using System.Xml.Serialization;

namespace dk.zunk.RemoteAttachIis
{
  public class ServerListConverter : TypeConverter
  {
    public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
    {
      if (typeof(string) == sourceType)
        return true;
      return base.CanConvertFrom(context, sourceType);
    }

    public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
    {
      if (typeof(string) == destinationType)
        return true;
      return base.CanConvertTo(context, destinationType);
    }

    public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
    {
      var str = value as string;
      if (str != null)
      {
        try
        {
          var reader = new StringReader(str);
          var serializer = new XmlSerializer(typeof(ServerList));
          return serializer.Deserialize(reader);
        }
        catch (Exception)
        {
          return ServerList.DefaultList;
        }
      }
      return base.ConvertFrom(context, culture, value);
    }

    public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
    {
      var serverList = value as ServerList;
      if (serverList != null && typeof(string) == destinationType)
      {
        try
        {
          var writer = new StringWriter();
          var serializer = new XmlSerializer(typeof(ServerList));
          serializer.Serialize(writer, serverList);
          return writer.ToString();
        }
        catch (Exception)
        {
          return "";
        }
      }
      return base.ConvertTo(context, culture, value, destinationType);
    }
  }

}
