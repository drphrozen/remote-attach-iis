﻿using System;
using System.Linq;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Shell;
using System.Windows.Forms;

namespace dk.zunk.RemoteAttachIis
{
  [Guid(GuidList.GuidOptionsDialogString)]
  [ClassInterface(ClassInterfaceType.AutoDual)]
  public class OptionPageCustom : DialogPage
  {
    public OptionPageCustom()
    {
      _window = new OptionPageCustomUserControl(this);
      _serverList = new ServerList();
    }

    private readonly IWin32Window _window;
    public event EventHandler<OptionsPageEventArgs> OptionsChanged;

    private ServerList _serverList;

    [Category(RemoteAttachIisPackage.OptionsCategoryName)]
    [DisplayName("IIS Servers")]
    [Description("List of IIS servers to attach to.")]
    [TypeConverter(typeof(ServerListConverter))]
    public ServerList Servers {
      get { return _serverList; }
      set {
        _serverList = value;
        var handler = OptionsChanged;
        if (handler != null)
          handler.Invoke(this, new OptionsPageEventArgs(this));
      }
    }

    protected override void OnApply(PageApplyEventArgs e)
    {
      if (Servers != null)
      {
        Servers.ClearAndAddRange(Servers.Items.Where(x => !string.IsNullOrWhiteSpace(x.Host) && !string.IsNullOrWhiteSpace(x.Qualifier)).ToList());
      }

      base.OnApply(e);
    }

    [Browsable(false)]
    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    protected override IWin32Window Window { get { return _window; } }
  }
}