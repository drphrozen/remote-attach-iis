﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnvDTE80;
using Microsoft.Web.Administration;

namespace dk.zunk.RemoteAttachIis
{
  public static class RemoteIis
  {
    public static IisInfo GetRemoteIisInfo(string server)
    {
      var serverManager = ServerManager.OpenRemote(server);
      var workerProcesses = serverManager.WorkerProcesses.Where(x => x.State == WorkerProcessState.Running).ToDictionary(x => x.AppPoolName);

      var result = serverManager.Sites.SelectMany(
        site =>
          {
            var bindings = string.Join(", ", site.Bindings.Select(x=> x.ToString()));
            return site.Applications.Select(
              app =>
              new ApplicationInfo
                {
                  SiteName = site.Name,
                  ApplicationPoolName = app.ApplicationPoolName,
                  ApplicationPath = app.Path,
                  Pid = workerProcesses.GetPid(app.ApplicationPoolName),
                  Bindings = bindings,
                  Server = server
                });
          });
      return new IisInfo {Applications = result.ToList()};
    }

    private static int GetPid(this IReadOnlyDictionary<string, WorkerProcess> workerProcesses,
                              string applicationPoolName)
    {
      WorkerProcess value;
      return workerProcesses.TryGetValue(applicationPoolName, out value)
               ? value.ProcessId
               : -1;
    }

    public static void Attach(this Debugger2 debugger, string server, string applicationPoolName, string transportQualifier)
    {
      var serverManager = ServerManager.OpenRemote(server);
      var applicationPool = serverManager.ApplicationPools.SingleOrDefault(x => x.Name == applicationPoolName);
      if (applicationPool == null) throw new Exception(string.Format(@"Could not find the application pool ""{0}"".", applicationPoolName));
      WorkerProcess workerProcess;
      try
      {
        workerProcess = applicationPool.WorkerProcesses.SingleOrDefault();
      }
      catch (Exception)
      {
        throw new Exception(string.Format(@"Multiple processes found for the application pool ""{0}"", at the moment only one process is supported!", applicationPoolName));
      }
      if (workerProcess == null) throw new Exception(string.Format(@"No process found for the application pool ""{0}"".", applicationPoolName));
      
      var processId = workerProcess.ProcessId;
      var transport = debugger.Transports.Item("Default");

      var remoteProcesses = debugger.GetProcesses(transport, transportQualifier);
      var process = remoteProcesses.Cast<Process2>().FirstOrDefault(x => x.ProcessID == processId);
      if (process == null) throw new Exception(string.Format("Could not debug a process with a PID of {0}.", processId));
      process.Attach2();
    }

    public static void Recycle(string server, string applicationPoolName)
    {
      var serverManager = ServerManager.OpenRemote(server);
      var applicationPool = serverManager.ApplicationPools.SingleOrDefault(x => x.Name == applicationPoolName);
      if (applicationPool == null) throw new Exception(string.Format(@"Could not find an application with the name ""{0}"".", applicationPoolName));
      applicationPool.Recycle();
    }
  }
}