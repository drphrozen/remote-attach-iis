﻿using System.Drawing;
using System.Windows.Forms;

namespace dk.zunk.RemoteAttachIis
{
  public partial class OptionPageCustomUserControl : UserControl
  {
    private readonly OptionPageCustom _optionPageCustom;

    public OptionPageCustomUserControl(OptionPageCustom optionPageCustom)
    {
      InitializeComponent();

      _optionPageCustom = optionPageCustom ?? new OptionPageCustom();
      _optionPageCustom.OptionsChanged += OptionPageCustom_OptionsChanged;

      if (_optionPageCustom.Servers == null)
        _optionPageCustom.Servers = ServerList.DefaultList;


      _serverDataGridView.DataError += ServerDataGridView_DataError;
      _serverDataGridView.MouseMove += ServerDataGridView_MouseMove;
      _serverDataGridView.MouseDown += ServerDataGridView_MouseDown;
      _serverDataGridView.DragDrop += ServerDataGridView_DragDrop;
      _serverDataGridView.DragOver += ServerDataGridView_DragOver;
      _serverDataGridView.CellEndEdit += ServerDataGridViewOnCellEndEdit;
    }

    private void ServerDataGridViewOnCellEndEdit(object sender, DataGridViewCellEventArgs args)
    {
      _serverDataGridView.InvalidateRow(args.RowIndex);
    }

    void ServerDataGridView_DataError(object sender, DataGridViewDataErrorEventArgs e)
    {
      e.ThrowException = false;
    }

    void OptionPageCustom_OptionsChanged(object sender, OptionsPageEventArgs e)
    {
      if (_optionPageCustom.Servers != null)
      {
        _serverDataGridView.DataSource = _optionPageCustom.Servers.Items;
      }
    }

    private Rectangle _dragBoxFromMouseDown;
    private int _rowIndexFromMouseDown;

    private void ServerDataGridView_MouseMove(object sender, MouseEventArgs e)
    {
      if ((e.Button & MouseButtons.Left) != MouseButtons.Left) return;
      // If the mouse moves outside the rectangle, start the drag.
      if (_dragBoxFromMouseDown != Rectangle.Empty &&
          !_dragBoxFromMouseDown.Contains(e.X, e.Y))
      {

        // Proceed with the drag and drop, passing in the list item.                    
        _serverDataGridView.DoDragDrop(_serverDataGridView.Rows[_rowIndexFromMouseDown], DragDropEffects.Move);
      }
    }

    private void ServerDataGridView_MouseDown(object sender, MouseEventArgs e)
    {
      // Get the index of the item the mouse is below.
      _rowIndexFromMouseDown = _serverDataGridView.HitTest(e.X, e.Y).RowIndex;
      if (_rowIndexFromMouseDown != -1)
      {
        // Remember the point where the mouse down occurred. 
        // The DragSize indicates the size that the mouse can move 
        // before a drag event should be started.                
        var dragSize = SystemInformation.DragSize;

        // Create a rectangle using the DragSize, with the mouse position being
        // at the center of the rectangle.
        _dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                       e.Y - (dragSize.Height / 2)),
                dragSize);
      }
      else
        // Reset the rectangle if the mouse is not over an item in the ListBox.
        _dragBoxFromMouseDown = Rectangle.Empty;
    }

    private void ServerDataGridView_DragOver(object sender, DragEventArgs e)
    {
      var rowIndex = GetRowIndexOfItemUnderMouseToDrop(e);
      
      e.Effect = rowIndex != -1
        ? DragDropEffects.Move
        : DragDropEffects.None;
    }

    private int GetRowIndexOfItemUnderMouseToDrop(DragEventArgs e)
    {
      // The mouse locations are relative to the screen, so they must be 
      // converted to client coordinates.
      var clientPoint = _serverDataGridView.PointToClient(new Point(e.X, e.Y));

      // Get the row index of the item the mouse is below. 
      return _serverDataGridView.HitTest(clientPoint.X, clientPoint.Y).RowIndex;
    }

    private void ServerDataGridView_DragDrop(object sender, DragEventArgs e)
    {
      var rowIndex = GetRowIndexOfItemUnderMouseToDrop(e);
      if (rowIndex == -1 || e.Effect != DragDropEffects.Move) return;

      var rowToMove = e.Data.GetData(typeof(DataGridViewRow)) as DataGridViewRow;
      if (rowToMove == null) return;
      var server = rowToMove.DataBoundItem as Server;
      if (server == null) return;
      var from = rowToMove.Index;
      var to = rowIndex;

      var items = _optionPageCustom.Servers.Items;

      var newIndex = to;
      if (newIndex > items.Count - 1)
        newIndex = items.Count - 1;
      if (newIndex < 0)
        newIndex = 0;

      items.RemoveAt(from);
      items.Insert(newIndex, server);
    }
  }
}
