﻿namespace dk.zunk.RemoteAttachIis
{
  static class PackageCommandIdList
  {
    public const uint CommandIdRemoteAttachIis = 0x100;
    public const uint CommandIdRemoteAttachIisWindow = 0x101;
  };
}