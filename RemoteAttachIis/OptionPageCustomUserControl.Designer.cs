﻿namespace dk.zunk.RemoteAttachIis
{
  partial class OptionPageCustomUserControl
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this._serverDataGridView = new System.Windows.Forms.DataGridView();
      ((System.ComponentModel.ISupportInitialize)(this._serverDataGridView)).BeginInit();
      this.SuspendLayout();
      // 
      // _serverDataGridView
      // 
      this._serverDataGridView.AllowDrop = true;
      this._serverDataGridView.AllowUserToResizeRows = false;
      this._serverDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
      this._serverDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
      this._serverDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this._serverDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
      this._serverDataGridView.Location = new System.Drawing.Point(0, 0);
      this._serverDataGridView.Name = "_serverDataGridView";
      this._serverDataGridView.Size = new System.Drawing.Size(564, 403);
      this._serverDataGridView.TabIndex = 0;
      // 
      // OptionPageCustomUserControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this._serverDataGridView);
      this.Name = "OptionPageCustomUserControl";
      this.Size = new System.Drawing.Size(564, 403);
      ((System.ComponentModel.ISupportInitialize)(this._serverDataGridView)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView _serverDataGridView;

  }
}
